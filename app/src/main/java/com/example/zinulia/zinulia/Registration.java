package com.example.zinulia.zinulia;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

public class Registration extends AppCompatActivity {

    private int DIALOG_DATE = 1;
    private int myYear = 1990;
    private int myMonth = 01;
    private int myDay = 01;
    private Button days;
    private Button months;
    private Button years;


    /** Called when the activity is first created. */

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);
        days = (Button) findViewById(R.id.day);
        months = (Button) findViewById(R.id.month);
        years = (Button) findViewById(R.id.year);
    }

    public void onclick(View view) {
        showDialog(DIALOG_DATE);
    }


    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_DATE) {
            DatePickerDialog tpd = new DatePickerDialog(this, myCallBack, myYear, myMonth, myDay);
            return tpd;
        }
        return super.onCreateDialog(id);
    }

    DatePickerDialog.OnDateSetListener myCallBack = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myYear = year;
            myMonth = monthOfYear;
            myDay = dayOfMonth;

            days.setText(toString().valueOf(myDay));

            if (myMonth > 0 || myMonth == 0)
            {
                months.setText(toString().valueOf(myMonth + 1));
            }
            years.setText(toString().valueOf(myYear));
        }
    };
}
