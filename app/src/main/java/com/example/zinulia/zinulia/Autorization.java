package com.example.zinulia.zinulia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Autorization extends AppCompatActivity {
    TextView registration;
    TextView restorepass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.autorization);

        registration = (TextView) findViewById(R.id.registration);
        restorepass = (TextView) findViewById(R.id.restorepassword);
    }

    public void onclick(View view) {
        switch (view.getId()) {
            case R.id.registration:
                Intent intentReg = new Intent(this, Registration.class);
                startActivity(intentReg);
                break;
            case R.id.restorepassword:
                Intent intentPass = new Intent(this, RestorePassword.class);
                startActivity(intentPass);
                break;
        }
    }

}
